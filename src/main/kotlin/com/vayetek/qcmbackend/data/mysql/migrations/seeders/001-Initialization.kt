package com.vayetek.qcmbackend.data.mysql.migrations.seeders

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.libraries.constants.Authority
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class Initialization {

    @Autowired
    lateinit var customerService: IUserService<Customer>

    @PostConstruct
    fun seed() {
        seedCustomerTable()
    }

    fun seedCustomerTable() {
        val customer = customerService.findOneByUsername("customer@vayetek.com")
        if (customer == null) {
            customerService.create(Customer(
                    firstName = "customer",
                    lastName = "vayetek",
                    phone = "23581532",
                    username = "yassine@vayetek.com",
                    password = "CustomerVayetek",
                    active = true,
                    authorities = mutableSetOf(Authority.ADMIN)
            ))
        }
    }
}
