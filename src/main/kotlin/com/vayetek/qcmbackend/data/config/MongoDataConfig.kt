package com.vayetek.qcmbackend.data.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

//TODO - check why this not work and remove from yml properties
@Configuration
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class,
    DataSourceTransactionManagerAutoConfiguration::class,
    HibernateJpaAutoConfiguration::class])
@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
class MongoDataConfig{

    @PostConstruct
    fun init() {
        System.out.println("MongoDataConfig")
    }
}