package com.vayetek.qcmbackend.data.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

//TODO - check why this not work and remove from yml properties
@Configuration
@EnableAutoConfiguration(exclude = [MongoAutoConfiguration::class,
    MongoRepositoriesAutoConfiguration::class,
    MongoDataAutoConfiguration::class])
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class JpaDataConfig {
    @PostConstruct
    fun init() {
        System.out.println("JpaDataCOnfig")
    }
}