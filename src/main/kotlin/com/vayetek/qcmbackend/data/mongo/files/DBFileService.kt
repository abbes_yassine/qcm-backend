package com.vayetek.qcmbackend.data.mongo.files


import com.mongodb.client.gridfs.GridFSBucket
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.io.InputStreamSource
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.GridFsResource
import org.springframework.data.mongodb.gridfs.GridFsTemplate
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.IOException


@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
@Service
class DBFileService {

    companion object {
        lateinit var placeholder: String
    }

    @Autowired
    lateinit var gridFsTemplate: GridFsTemplate


    @Autowired
    lateinit var gridFSBucket: GridFSBucket

    @Throws(IOException::class)
    fun save(file: MultipartFile, fileMetadata: DBFileMetadata?): DBFile {
        val gridFsFileId = gridFsTemplate.store(file.inputStream, file.originalFilename, file.contentType, fileMetadata?.metadatas)
        return get(gridFsFileId.toHexString())
    }


    @Throws(IOException::class)
    fun save(file: DBFile, fileMetadata: DBFileMetadata?): DBFile {
        val gridFsFileId = gridFsTemplate.store(file.inputStream
                ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND), file.filename, file.contentType, fileMetadata?.metadatas)
        return get(gridFsFileId.toString())
    }

    fun get(id: String): DBFile =
            gridFsTemplate.findOne(Query().addCriteria(Criteria.where("_id").`is`(id)))
                    .let {
                        val gridFSDownloadStream = gridFSBucket.openDownloadStream(it.id)
                        val gridFsResource = GridFsResource(it, gridFSDownloadStream)
                        val file = GridFSConverters.toDBFile(it)
                        file.inputStream = gridFsResource.inputStream
                        file
                    }

    fun remove(id: String) =
            gridFsTemplate.findOne(Query().addCriteria(Criteria.where("_id").`is`(id)))
                    .apply { gridFsTemplate.delete(Query().addCriteria(Criteria.where("_id").`is`(id))) }

    fun getContent(id: String): InputStreamSource? {
        return InputStreamSource {
            this.get(id).inputStream ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND)
        }
    }
}
