package com.vayetek.qcmbackend.data.mongo.converter.date


import com.vayetek.qcmbackend.data.mongo.converter.MongoDBConverters
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@ConditionalOnProperty("vayetek.libraries.mongodb.dates.enabled")
@Configuration
class MongoDateConfiguration {

    private val log = LoggerFactory.getLogger(MongoDateConfiguration::class.java)

    @Bean
    fun dateConverters(): MongoDBConverters {
        log.info("Adding MongoDB.DateTime converters")
        val converters = MongoDBConverters()
        converters.add(DateConverters.DATE_TO_ZONED_DATE_TIME)
        converters.add(DateConverters.ZONED_DATE_TIME_TO_DATE)
        converters.add(DateConverters.DATE_TO_LOCALE_DATE)
        converters.add(DateConverters.LOCAL_DATE_TO_DATE)
        return converters
    }
}
