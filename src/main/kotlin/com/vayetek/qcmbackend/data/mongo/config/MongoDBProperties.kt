package com.vayetek.qcmbackend.data.mongo.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("vayetek.libraries.mongodb", ignoreUnknownFields = true)
class MongoDBProperties {

    var enabled: Boolean = false

    var files: FilesProperties = FilesProperties()

    var dates: DatesProperties = DatesProperties()

    var migrations: MigrationsProperties = MigrationsProperties()

    class FilesProperties {

        var enabled: Boolean = false
        var relativeUrl: String = ""
        var fullUrl: String = ""
        var placeholder: String = ""
    }

    class DatesProperties {

        var enabled: Boolean = false

    }

    class MigrationsProperties {

        var enabled: Boolean = false

        var `package`: String = ""
    }
}
