package com.vayetek.qcmbackend.data.mongo.converter.date

import com.vayetek.qcmbackend.libraries.utilis.extensions.toDate
import com.vayetek.qcmbackend.libraries.utilis.extensions.toLocalDate
import com.vayetek.qcmbackend.libraries.utilis.extensions.toLocalDateTime
import com.vayetek.qcmbackend.libraries.utilis.extensions.toZonedDateTime
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateConverters {

    val LOCAL_DATE_TO_DATE = LocalDateToDateConverter()
    var DATE_TO_LOCALE_DATE = DateToLocalDateConverter()
    val ZONED_DATE_TIME_TO_DATE = ZonedDateTimeToDateConverter()
    val DATE_TO_ZONED_DATE_TIME = DateToZonedDateTimeConverter()
    val LOCALE_DATE_TIME_TO_DATE = LocalDateTimeToDateConverter()
    val DATE_TO_LOCAL_DATE_TIME = DateToLocalDateTimeConverter()

    val LOCAL_DATE_PARSERS = arrayOf(
            DateTimeFormatter.ISO_LOCAL_DATE,
            DateTimeFormatter.ISO_INSTANT
    )

    @Configuration
    class LocalDateToDateConverter : Converter<LocalDate, Date> {
        override fun convert(source: LocalDate): Date? = source.toDate()
    }

    @Configuration
    class DateToLocalDateConverter : Converter<Date, LocalDate> {
        override fun convert(source: Date): LocalDate? = source.toLocalDate()
    }

    @Configuration
    class ZonedDateTimeToDateConverter : Converter<ZonedDateTime, Date> {
        override fun convert(source: ZonedDateTime): Date? = source.toDate()
    }

    @Configuration
    class DateToZonedDateTimeConverter : Converter<Date, ZonedDateTime> {
        override fun convert(source: Date): ZonedDateTime? = source.toZonedDateTime()
    }

    @Configuration
    class LocalDateTimeToDateConverter : Converter<LocalDateTime, Date> {
        override fun convert(source: LocalDateTime): Date? = source.toDate()
    }

    @Configuration
    class DateToLocalDateTimeConverter : Converter<Date, LocalDateTime> {
        override fun convert(source: Date): LocalDateTime? = source.toLocalDateTime()
    }

    @Configuration
    class StringToLocalDate : Converter<String, LocalDate> {

        override fun convert(source: String): LocalDate? {
            LOCAL_DATE_PARSERS.forEach {
                try {
                    return LocalDate.parse(source, it)
                } catch (e: Exception) {
                }
            }
            return null
        }
    }
}
