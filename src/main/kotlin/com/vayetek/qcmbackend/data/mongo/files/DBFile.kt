package com.vayetek.qcmbackend.data.mongo.files

import com.fasterxml.jackson.annotation.JsonIgnore
import com.mongodb.client.gridfs.model.GridFSFile
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import org.springframework.core.io.InputStreamResource
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import java.io.InputStream

@Document(collection = "fs.files")
class DBFile() {

    @Id
    var id: String? = null

    var filename: String? = null

    var contentType: String? = null

    var length: Long = 0

    var path: String? = null
    var relative: String? = null

    @JsonIgnore
    var inputStream: InputStream? = null

    @JsonIgnore
    var metadatas = DBFileMetadata()

    constructor(file: GridFSFile) : this() {
        this.id = file.id.asObjectId().value.toHexString()
        this.filename = file.filename
        this.contentType = file.metadata.getString("_contentType")
        this.length = file.length
        this.metadatas = DBFileMetadata(file.metadata)
    }

    @JsonIgnore
    fun toResponse(): ResponseEntity<InputStreamResource> {
        val res = InputStreamResource(inputStream ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND))
        val httpHeaders = HttpHeaders()
        httpHeaders.contentLength = length
        httpHeaders.contentType = MediaType.parseMediaType(contentType
                ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND))
        return ResponseEntity(res, httpHeaders, HttpStatus.OK)
    }

    //fun toURL(): String = "http://${InetAddress.getLocalHost().hostAddress}:8080$relative/$filename"
    fun toURL(): String = "$path/$filename"
}
