package com.vayetek.qcmbackend.data.mongo.files

import com.vayetek.qcmbackend.data.mongo.config.MongoDBProperties
import com.vayetek.qcmbackend.data.mongo.converter.MongoDBConverters
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@ConditionalOnProperty("vayetek.libraries.mongodb.files.enabled")
@Configuration
class DBFileConfiguration {

    private val log = LoggerFactory.getLogger(DBFileConfiguration::class.java)

    @Autowired
    lateinit var mongoProperties: MongoDBProperties

    @PostConstruct
    fun initialize() {
        GridFSConverters.relativeUrl = mongoProperties.files.relativeUrl
        GridFSConverters.fullUrl = mongoProperties.files.fullUrl
        DBFileService.placeholder = mongoProperties.files.placeholder
    }

    @Bean
    fun dbFilesConverters(): MongoDBConverters {
        log.info("Adding MongoDB.Files converters")
        val converters = MongoDBConverters()
        converters.add(GridFSConverters.gridFS)
        return converters
    }
}
