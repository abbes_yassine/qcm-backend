package com.vayetek.qcmbackend.data.mongo.migrations.seeders

import com.github.mongobee.changeset.ChangeLog
import com.github.mongobee.changeset.ChangeSet
import com.vayetek.qcmbackend.api.user.model.User
import com.vayetek.qcmbackend.libraries.constants.Authority
import org.springframework.data.mongodb.core.MongoTemplate

@ChangeLog(order = "001")
class Initialization {

    @ChangeSet(order = "01", author = "initiator", id = "01-create-admin-user")
    fun intialize(mongoTemplate: MongoTemplate) {
        mongoTemplate.save(User(
                username = "admin@vayetek.com",
                password = "\$2a\$04\$EM/aJtOUBnkxRpjv8vGK9OlQYLXMHX9nuTFrepkH/QBGik0MywRTC",// AdminVayetek,
                active = true,
                authorities = mutableSetOf(Authority.ADMIN)
        ))
    }
}
