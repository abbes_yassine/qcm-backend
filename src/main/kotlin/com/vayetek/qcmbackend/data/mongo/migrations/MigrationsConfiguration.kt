package com.vayetek.qcmbackend.data.mongo.migrations

import com.github.mongobee.Mongobee
import com.mongodb.MongoClient
import com.vayetek.qcmbackend.data.mongo.config.MongoDBProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.BeanInitializationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate

@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
@Configuration
class MigrationsConfiguration {

    val LOG: Logger = LoggerFactory.getLogger(MigrationsConfiguration::class.java)

    @Autowired
    lateinit var properties: MongoDBProperties

    @Bean
    fun mongobee(mongoClient: MongoClient, mongoTemplate: MongoTemplate, mongoProperties: MongoProperties): Mongobee {
        LOG.debug("Configuring Mongobee")

        if (properties.migrations.`package`.isEmpty())
            throw BeanInitializationException("Package name is empty. Please provide value in 'vayetek.libraries.mongodb.migrations.package'")

        val mongobee = Mongobee(mongoClient)
        mongobee.setDbName(mongoProperties.database ?: "test")
        mongobee.setMongoTemplate(mongoTemplate)
        // package to scan for migrations
        mongobee.setChangeLogsScanPackage(properties.migrations.`package`)
        mongobee.isEnabled = true
        return mongobee
    }
}
