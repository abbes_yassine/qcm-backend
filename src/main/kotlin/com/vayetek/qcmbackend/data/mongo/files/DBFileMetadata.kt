package com.vayetek.qcmbackend.data.mongo.files

import com.mongodb.BasicDBObject
import org.bson.Document
import java.util.*
import kotlin.collections.ArrayList

class DBFileMetadata() {

    enum class Metadata {
        ACCESS_CONTROL_USERS,
        ACCESS_CONTROL_AUTHORITIES,
        ACCESS_CONTROL_TOKEN
    }

    val metadatas: BasicDBObject
        get() {
            val me = BasicDBObject()
            me.put(Metadata.ACCESS_CONTROL_USERS.name, this.users)
            me.put(Metadata.ACCESS_CONTROL_AUTHORITIES.name, this.authorities)
            me.put(Metadata.ACCESS_CONTROL_TOKEN.name, this.token)
            return me
        }

    var token: String? = null
    var users: MutableList<String>? = null
    var authorities: MutableList<String>? = null

    @Suppress("UNCHECKED_CAST")
    constructor(metadatas: Document) : this() {
        this.token = metadatas[Metadata.ACCESS_CONTROL_TOKEN.name] as String?
        this.users = metadatas[Metadata.ACCESS_CONTROL_USERS.name] as MutableList<String>?
        this.authorities = metadatas[Metadata.ACCESS_CONTROL_AUTHORITIES.name] as MutableList<String>?
    }

    fun generateToken(): DBFileMetadata {
        val token = this.token ?: UUID.randomUUID().toString()
        this.token = token
        return this
    }

    fun allowUser(user: String): DBFileMetadata {
        generateToken()
        val users = this.users ?: ArrayList()

        if (!users.contains(user))
            users.add(user)

        this.users = users
        return this
    }

    fun allowAuthority(authority: String): DBFileMetadata {
        generateToken()
        val authorities = this.authorities ?: ArrayList()

        if (!authorities.contains(authority))
            authorities.add(authority)

        this.authorities = authorities
        return this
    }

    /**
     * Transient
     */
    fun hasACL(): Boolean {
        return token != null || users != null && (users != null && users!!.size > 0) || (authorities != null && authorities!!.size > 0)
    }

    fun withUser(user: String): Boolean {
        return users == null || users?.contains(user) == true
    }

    fun withAuthority(authority: String): Boolean {
        return authorities == null || authorities?.contains(authority) == true
    }

    fun withAuthorities(authorities: List<String>): Boolean {
        for (authority in authorities) {
            if (withAuthority(authority))
                return true
        }
        return false
    }

    fun withToken(token: String): Boolean {
        return this.token.isNullOrEmpty() || this.token.equals(token)
    }
}
