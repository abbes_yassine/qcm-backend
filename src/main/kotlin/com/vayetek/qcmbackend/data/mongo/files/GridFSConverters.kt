package com.vayetek.qcmbackend.data.mongo.files

import com.mongodb.client.gridfs.model.GridFSFile
import org.springframework.core.convert.converter.Converter

object GridFSConverters {
    val gridFS = GridFSFileToDBFile()

    fun toDBFile(file: GridFSFile): DBFile {
        return gridFS.convert(file)
    }

    lateinit var relativeUrl: String
    lateinit var fullUrl: String

    class GridFSFileToDBFile : Converter<GridFSFile, DBFile> {

        override fun convert(source: GridFSFile): DBFile {
            val file = DBFile(source)
            var fileUrl = file.id

            if (file.metadatas.token != null)
                fileUrl += "?token=" + file.metadatas.token

            file.relative = "$relativeUrl/$fileUrl"
            file.path = "$fullUrl/$fileUrl"
            return file
        }
    }
}
