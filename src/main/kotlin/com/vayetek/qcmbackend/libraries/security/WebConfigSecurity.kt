package com.vayetek.qcmbackend.libraries.security

import com.vayetek.qcmbackend.libraries.security.authentication.AuthenticationFilter
import org.springframework.beans.factory.BeanInitializationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class WebConfigSecurity : WebSecurityConfigurerAdapter() {


    @Autowired
    lateinit var authenticationFilter: AuthenticationFilter

    @Autowired
    lateinit var configurers: List<IHttpSecurityConfigurer>

    @Autowired
    lateinit var userDetailsService: UserDetailsService

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder


    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.TRACE, "/**").denyAll()
                .and()
                // And filter other requests to check the presence of JWT in header
                .addFilterBefore(authenticationFilter,
                        UsernamePasswordAuthenticationFilter::class.java)

        configurers.forEach { configurer -> configurer.configure(http) }

    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        try {
            auth
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder)
        } catch (e: Exception) {
            throw BeanInitializationException("Security configuration failed", e)
        }
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }
}