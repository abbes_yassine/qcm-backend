package com.vayetek.qcmbackend.libraries.security.data.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "tokens")
data
class MongoPersistentToken(
        @Id var token: String,
        @Indexed var principal: String
)
