package com.vayetek.qcmbackend.libraries.security.authentication

import org.springframework.security.core.token.Token

interface ITokenPersistence<T> {

    fun exists(token: Token): Boolean
    fun save(token: Token, principal: String): T
    fun remove(token: Token): Boolean

}
