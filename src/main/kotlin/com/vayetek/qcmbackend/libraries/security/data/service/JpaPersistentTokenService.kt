package com.vayetek.qcmbackend.libraries.security.data.service

import com.vayetek.qcmbackend.libraries.security.authentication.ITokenPersistence
import com.vayetek.qcmbackend.libraries.security.data.entity.JpaPersistentToken
import com.vayetek.qcmbackend.libraries.security.data.repository.JpaPersistentTokenRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.core.token.Token
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class JpaPersistentTokenService : ITokenPersistence<JpaPersistentToken> {

    val LOG: Logger = LoggerFactory.getLogger(JpaPersistentTokenService::class.java)

    @Autowired
    lateinit var persistentTokenRepository: JpaPersistentTokenRepository

    override fun exists(token: Token): Boolean {
        return persistentTokenRepository.findById(token.key).orElseGet { null } != null
    }

    override fun save(token: Token, principal: String): JpaPersistentToken {
        return persistentTokenRepository.save(JpaPersistentToken(token.key, principal))
    }

    override fun remove(token: Token): Boolean {
        persistentTokenRepository.deleteById(token.key)
        return true
    }

    fun removeAll(principal: String) {
        LOG.info("Cleaning tokens for $principal")
        persistentTokenRepository.deleteAll(persistentTokenRepository.findByPrincipal(principal))
    }
}
