package com.vayetek.qcmbackend.libraries.security.data.service

import com.vayetek.qcmbackend.libraries.security.authentication.ITokenPersistence
import com.vayetek.qcmbackend.libraries.security.data.entity.MongoPersistentToken
import com.vayetek.qcmbackend.libraries.security.data.repository.MongoPersistentTokenRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.core.token.Token
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
class MongoPersistentTokenService : ITokenPersistence<MongoPersistentToken> {

    val LOG: Logger = LoggerFactory.getLogger(MongoPersistentTokenService::class.java)

    @Autowired
    lateinit var mongoPersistentTokenRepository: MongoPersistentTokenRepository

    override fun exists(token: Token): Boolean {
        return mongoPersistentTokenRepository.findById(token.key).orElseGet { null } != null
    }

    override fun save(token: Token, principal: String): MongoPersistentToken {
        return mongoPersistentTokenRepository.save(MongoPersistentToken(token.key, principal))
    }

    override fun remove(token: Token): Boolean {
        mongoPersistentTokenRepository.deleteById(token.key)
        return true
    }

    fun removeAll(principal: String) {
        LOG.info("Cleaning tokens for $principal")
        mongoPersistentTokenRepository.deleteAll(mongoPersistentTokenRepository.findByPrincipal(principal))
    }
}
