package com.vayetek.qcmbackend.libraries.security.data.repository

import com.vayetek.qcmbackend.libraries.security.data.entity.JpaPersistentToken
import org.springframework.data.jpa.repository.JpaRepository

interface JpaPersistentTokenRepository : JpaRepository<JpaPersistentToken, String> {

    fun findByPrincipal(principal: String): List<JpaPersistentToken>

}
