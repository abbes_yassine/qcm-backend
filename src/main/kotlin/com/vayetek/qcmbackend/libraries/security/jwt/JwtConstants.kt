package com.vayetek.qcmbackend.libraries.security.jwt

object JwtConstants {
    const val AUTHORITIES_KEY = "auth"
    const val ACCESS_TOKEN_KEY = "accessToken"
    const val IS_REFRESH_KEY = "isRefreshToken"
}