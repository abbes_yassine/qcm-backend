package com.vayetek.qcmbackend.libraries.security.data.repository

import com.vayetek.qcmbackend.libraries.security.data.entity.MongoPersistentToken
import org.springframework.data.mongodb.repository.MongoRepository

interface MongoPersistentTokenRepository : MongoRepository<MongoPersistentToken, String> {

    fun findByPrincipal(principal: String): List<MongoPersistentToken>
}
