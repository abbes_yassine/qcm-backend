package com.vayetek.qcmbackend.libraries.security

import org.springframework.security.config.annotation.web.builders.HttpSecurity

interface IHttpSecurityConfigurer {
    fun configure(http: HttpSecurity?)
}