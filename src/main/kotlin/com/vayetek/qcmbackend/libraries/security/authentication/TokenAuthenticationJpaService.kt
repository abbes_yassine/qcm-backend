package com.vayetek.qcmbackend.libraries.security.authentication

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.libraries.security.data.entity.JpaPersistentToken
import com.vayetek.qcmbackend.libraries.security.jwt.JwtConstants
import com.vayetek.qcmbackend.libraries.security.jwt.JwtProperties
import com.vayetek.qcmbackend.libraries.security.jwt.JwtToken
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.token.Token
import org.springframework.stereotype.Component
import java.security.ProviderException
import java.util.*
import java.util.stream.Collectors
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class TokenAuthenticationJpaService : ITokenService {

    private val log = LoggerFactory.getLogger(TokenAuthenticationJpaService::class.java)

    val TOKEN_PREFIX = "Bearer"
    val HEADER_STRING = "Authorization"

    @Autowired
    lateinit var properties: JwtProperties

    @Autowired
    lateinit var userService: IUserService<Customer>

    @Autowired
    lateinit var persistence: ITokenPersistence<JpaPersistentToken>

    override fun verifyToken(key: String?): Token {
        try {
            return this.unpackToken(key)
        } catch (e: SignatureException) {
            log?.info("Invalid JWT signature.")
            log.trace("Invalid JWT signature trace: {}", e)
        } catch (e: MalformedJwtException) {
            log.info("Invalid JWT token.")
            log.trace("Invalid JWT token trace: {}", e)
        } catch (e: ExpiredJwtException) {
            log.info("Expired JWT token.")
            log.trace("Expired JWT token trace: {}", e)
        } catch (e: UnsupportedJwtException) {
            log.info("Unsupported JWT token.")
            log.trace("Unsupported JWT token trace: {}", e)
        } catch (e: IllegalArgumentException) {
            log.info("JWT token compact of handler are invalid.")
            log.trace("JWT token compact of handler are invalid trace: {}", e)
        }
        return JwtToken("")
    }

    private fun unpackToken(_token: String?): JwtToken {
        val token = JwtToken(properties.secretKey)
        token.token = _token
        return token
    }

    override fun allocateToken(token: String?): Token {
        return saveToken(unpackToken(token))
    }

    private fun saveToken(token: JwtToken): JwtToken {
        persistence.save(token, token.getSubject())
        return token
    }

    override fun addAuthentication(res: HttpServletResponse, username: String) {
        val JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(Date(System.currentTimeMillis() + properties.timeToLive))
                .signWith(SignatureAlgorithm.HS512, properties.secretKey)
                .compact()
        res.addHeader(HEADER_STRING, "$TOKEN_PREFIX $JWT")
    }

    override fun getAuthentication(request: HttpServletRequest): Authentication? {
        val token = request.getHeader(HEADER_STRING)
        if (token != null) {
            // parse the token.
            return try {
                val user = Jwts.parser()
                        .setSigningKey(properties.secretKey)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .body
                        .subject
                if (user != null) {
                    val authorities = userService.findOneByPrincipal(user)?.roles
                    UsernamePasswordAuthenticationToken(user, null, authorities)
                } else
                    null
            } catch (exception: MalformedJwtException) {
                null
            }
        }
        return null
    }

    override fun revokeToken(token: String) {
        if (!persistence.remove(this.verifyToken(token)))
            throw ProviderException("Unable to remove token")
    }

    override fun allocateAccessToken(authentication: Authentication): Token {
        val authorities =
                authentication.authorities.stream()
                        .map { it.authority }
                        .collect(Collectors.joining(","))

        val validity = Date(Date().time + properties.timeToLive)

        val token = JwtToken(properties.secretKey)
        val key = Jwts.builder()
                .setSubject(authentication.name)
                .claim(JwtConstants.AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS512, properties.secretKey)
                .setExpiration(validity)
                .compact()
        token.token = key
        return saveToken(token)
    }
}