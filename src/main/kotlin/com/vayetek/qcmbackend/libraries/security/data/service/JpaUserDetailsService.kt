package com.vayetek.qcmbackend.libraries.security.data.service

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.libraries.constants.DomainExceptions
import com.vayetek.qcmbackend.libraries.errors.APIException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import java.util.*

@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
@Component
class JpaUserDetailsService : UserDetailsService {

    private val log = LoggerFactory.getLogger(JpaUserDetailsService::class.java)

    @Autowired
    lateinit var userService: IUserService<Customer>

    override fun loadUserByUsername(login: String): UserDetails {
        val lowercaseEmail = login.toLowerCase(Locale.ENGLISH)
        val user = userService.findOneByPrincipal(lowercaseEmail)
                ?: throw UsernameNotFoundException("User $lowercaseEmail was not found in the database")
        if (!user.canAuthenticate()) {
            throw APIException(DomainExceptions.USER_NOT_ACTIVATED)
        }
        return org.springframework.security.core.userdetails.User(lowercaseEmail, user.getSecret(), user.roles)
    }
}
