package com.vayetek.qcmbackend.libraries.security.jwt

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("vayetek.libraries.security.jwt", ignoreUnknownFields = false)
class JwtProperties {

    var enabled: Boolean = false

    var secretKey: String = ""

    // 1 hour
    var timeToLive: Long = (60L * 60 * 1000)

    // 30 days
    var rememberTimeToLive: Long = 30L * 24 * 60 * 60 * 1000

}
