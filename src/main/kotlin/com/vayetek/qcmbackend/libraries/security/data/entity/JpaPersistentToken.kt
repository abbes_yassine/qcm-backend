package com.vayetek.qcmbackend.libraries.security.data.entity

import org.springframework.data.mongodb.core.index.Indexed
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "token")
data
class JpaPersistentToken(
        @Id
        var token: String,
        @Indexed var principal: String
) {
    // TODO -  Remove this and handle No Default Constructor found
    constructor() : this("", "")
}
