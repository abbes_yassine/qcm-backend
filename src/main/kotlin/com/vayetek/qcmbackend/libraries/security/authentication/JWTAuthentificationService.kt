package com.vayetek.qcmbackend.libraries.security.authentication

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component


@Component
class JWTAuthentificationService : IAuthentificationService {

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var tokenService: ITokenService


    override fun authenticate(auth: UsernamePasswordAuthenticationToken): IAuthentificationService.AuthenticationResult {
        val authentication = authenticationManager.authenticate(auth)
        SecurityContextHolder.getContext().authentication = authentication
        val jwt = tokenService.allocateAccessToken(authentication)
        return IAuthentificationService.AuthenticationResult(jwt.key)
    }

    override fun logout(accessTokenString: String, refreshTokenString: String) {
        tokenService.revokeToken(accessTokenString)
        tokenService.revokeToken(refreshTokenString)
    }
}