package com.vayetek.qcmbackend.libraries.utilis

object ValidationConstants {
    const val PACKAGE_NAME = "com.vayetek.backend"

    //Regex for acceptable logins
    const val MONGOID_REGEX = "^[a-f0-9]{24}$"
    const val ZIPCODE_REGEX = "^[0-9]{5}(?:-[0-9]{4})?$"
    const val PHONE_REGEX = "^[0-9]{10}$"
    const val NUMERIC_REGEXP = "^[0-9]*$"
    const val ALPHA_HYPHEN_REGEX = "^[_A-Za-z0-9-]*$"

    val IMAGES_FILES_MIME_TYPE = arrayOf("image/jpeg", "image/jpg", "image/png", "image/bmp")
    val VIDEO_FILES_MIME_TYPE = arrayOf("video/avi", "video/quicktime", "video/mpeg", "video/ogg", "video/webm", "video/mp4", "video/3gp", "video/mov", "video/3gpp", "video/3gpp2", "video/x-m4v")

    /**
     * User
     */

    const val USER_DEFAULT_MAX_LENGTH = 255

    const val USER_EMAIL_MIN_LENGTH = 5
    const val USER_EMAIL_MAX_LENGTH = USER_DEFAULT_MAX_LENGTH

    const val USER_PASSWORD_MIN_LENGTH = 8
    const val USER_PASSWORD_MAX_LENGTH = 50


}
