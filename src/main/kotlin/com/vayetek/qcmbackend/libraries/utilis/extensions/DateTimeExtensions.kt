package com.vayetek.qcmbackend.libraries.utilis.extensions

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

fun ZonedDateTime.toDate(): Date = Date.from(this.toInstant())
fun ZonedDateTime.resetSeconds(): ZonedDateTime {
    return this
            .withSecond(0)
            .withNano(0)
}
fun ZonedDateTime.resetMinutes(): ZonedDateTime {
    return this.resetSeconds().withMinute(0)
}
fun ZonedDateTime.resetTime(): ZonedDateTime {
    return this.resetMinutes().withHour(0)
}

fun LocalDate.toDate(): Date = Date.from(this.atStartOfDay(ZoneId.systemDefault()).toInstant())
fun LocalDateTime.toDate(): Date = Date.from(this.atZone(ZoneId.systemDefault()).toInstant())

fun Date.toLocalDate(): LocalDate = ZonedDateTime.ofInstant(this.toInstant(), ZoneId.systemDefault()).toLocalDate()
fun Date.toZonedDateTime(): ZonedDateTime = ZonedDateTime.ofInstant(this.toInstant(), ZoneId.systemDefault())
fun Date.toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(this.toInstant(), ZoneId.systemDefault())
