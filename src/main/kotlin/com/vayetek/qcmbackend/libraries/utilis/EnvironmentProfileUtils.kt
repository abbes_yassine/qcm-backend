package com.vayetek.qcmbackend.libraries.utilis

import org.springframework.boot.SpringApplication
import org.springframework.core.env.Environment
import java.util.*

object EnvironmentProfileUtils {
    private val SPRING_PROFILE_DEFAULT = "spring.profiles.default"

    fun addDefaultProfile(app: SpringApplication, defaultProfile: String) {
        val defProperties = HashMap<String, Any>()
        /*
        * The default profile to use when no other profiles are defined
        * This cannot be set in the <code>application.yml</code> file.
        * See https://github.com/spring-projects/spring-boot/issues/1219
        */
        defProperties[SPRING_PROFILE_DEFAULT] = defaultProfile
        app.setDefaultProperties(defProperties)
    }

    /**
     * Get the profiles that are applied else get default profiles.
     *
     * @param env spring environment
     * @return profiles
     */
    fun getActiveProfiles(env: Environment): Array<String> {
        val profiles = env.activeProfiles
        return if (profiles.isEmpty()) {
            env.defaultProfiles
        } else profiles
    }
}