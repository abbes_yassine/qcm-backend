package com.vayetek.qcmbackend.libraries.utilis

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class Updatable(
        val nullable: Boolean = false,
        val inception: Boolean = false)
