package com.vayetek.qcmbackend.libraries.constants

enum class Authority {
    USER,
    MANAGER,
    ADMIN;

    val role: String by lazy { "ROLE_$this" }

    object Constants {
        const val ADMIN = "ADMIN"
        const val MANAGER = "MANAGER"
        const val USER = "USER"
    }
}
