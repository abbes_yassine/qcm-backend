package com.vayetek.qcmbackend.api.welcome.model.response

data class CustomerLoginResponse(val message: String)