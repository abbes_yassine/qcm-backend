package com.vayetek.qcmbackend.api.customer.model.request

import org.jetbrains.annotations.NotNull

class CustomerLoginRequest {

    @NotNull
    lateinit var email: String

    @NotNull
    lateinit var password: String
}