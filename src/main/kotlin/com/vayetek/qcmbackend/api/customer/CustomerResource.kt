package com.vayetek.qcmbackend.api.customer

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.customer.model.request.CustomerLoginRequest
import com.vayetek.qcmbackend.api.customer.model.request.CustomerRegistrationRequest
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.libraries.constants.DomainExceptions
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import com.vayetek.qcmbackend.libraries.security.authentication.IAuthentificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/customer")
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class CustomerResource {

    @Autowired
    lateinit var customerService: IUserService<Customer>
    @Autowired
    lateinit var jwtAuthentificationService: IAuthentificationService

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<Customer> {
        return customerService.get(page)
    }

    @GetMapping("me")
    @ResponseStatus(HttpStatus.OK)
    fun me(): Customer {
        return customerService.getConnectedUser() ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND)
    }

    @PostMapping("guest/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerCustomer(@Valid @RequestBody creation: CustomerRegistrationRequest): Customer {

        val request = creation.toCustomer()

        if (!customerService.isUsernameAvailable(request.username))
            throw APIException(DomainExceptions.ACCOUNT_ALREADY_EXISTS)

        return customerService.create(request)
    }


    @PostMapping("guest/login")
    @ResponseStatus(HttpStatus.OK)
    fun authenticate(@Valid @RequestBody login: CustomerLoginRequest): IAuthentificationService.AuthenticationResult {
        val authenticationToken = UsernamePasswordAuthenticationToken(login.email, login.password)
        return jwtAuthentificationService.authenticate(authenticationToken).let {
            it.user = customerService.getConnectedUser()
            it
        }
    }
}