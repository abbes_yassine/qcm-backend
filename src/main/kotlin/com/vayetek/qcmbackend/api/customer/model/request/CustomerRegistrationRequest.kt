package com.vayetek.qcmbackend.api.customer.model.request

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.user.model.request.validator.AuthoritiesRequestValidator
import com.vayetek.qcmbackend.libraries.constants.Authority
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

class CustomerRegistrationRequest {

    @NotNull
    lateinit var firstName: String

    @NotNull
    lateinit var lastName: String

    @Email
    @NotNull
    lateinit var email: String

    @NotNull
    lateinit var password: String

    @NotNull
    lateinit var phone: String

    @NotNull
    @AuthoritiesRequestValidator
    var authorities: MutableSet<Authority>? = mutableSetOf()

    fun toCustomer() = Customer(
            firstName = firstName,
            lastName = lastName,
            username = email.trim().toLowerCase(),
            phone = phone,
            password = password,
            authorities = authorities)
}