package com.vayetek.qcmbackend.api.customer.service

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.customer.service.repository.CustomerRepository
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.libraries.constants.Authority
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import com.vayetek.qcmbackend.libraries.security.jwt.SecurityUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service


@Service
@ConditionalOnProperty(name = ["spring.database"], havingValue = "jpa")
class CustomerService : IUserService<Customer> {
    override fun findOneByUsername(username: String): Customer? {
        val customer = repository.findOneByUsername(username)
        return if (customer.isPresent) {
            customer.get()
        } else {
            null
        }
    }


    val LOG: Logger = LoggerFactory.getLogger(CustomerService::class.java)

    @Autowired
    lateinit var repository: CustomerRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder


    override fun getConnectedUser(): Customer? {
        val login = SecurityUtils.getCurrentUserLogin() ?: return null
        return findOneByUsername(login)
    }

    fun get(id: Int): Customer {
        val customer = repository.findById(id)
        if (customer.isPresent) {
            return customer.get()
        } else {
            throw APIException(BaseExceptions.RESOURCE_NOT_FOUND)
        }
    }

    override fun isUsernameAvailable(username: String, connected: Customer?): Boolean {
        val existing = repository.findOneByUsername(username)
        return !existing.isPresent || existing.get().id == connected?.id
    }

    override fun findOneByPrincipal(principal: String): Customer? = repository.findOneByUsername(principal).get()

    override fun get(page: Pageable): Page<Customer> {
        return repository.findAll(page)
    }

    override fun create(toRegister: Customer, authorities: MutableSet<Authority>?): Customer {
        LOG.info("Creating user ${toRegister.username}")
        val user = toRegister.copy(password = passwordEncoder.encode(toRegister.password))
        return repository.save(user)
    }

    override fun update(base: Customer, update: Customer): Customer {
        LOG.info("Updating customer ${base.username}")
        val customer = repository.findById(base.id ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND))
        if (!customer.isPresent) {
            throw throw APIException(BaseExceptions.RESOURCE_NOT_FOUND)
        }
        return repository.save(base)
    }

    override fun delete(user: Customer) {
        repository.delete(user)
    }
}