package com.vayetek.qcmbackend.api.customer.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.vayetek.qcmbackend.libraries.constants.Authority
import com.vayetek.qcmbackend.libraries.security.data.entity.IUser
import com.vayetek.qcmbackend.libraries.utilis.Utils
import org.hibernate.validator.constraints.UniqueElements
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Customer")
data class Customer(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        @Column(name = "last_name")
        var firstName: String,

        @Column(name = "first_name")
        var lastName: String,

        var phone: String,

        @UniqueElements
        val username: String,

        @JsonIgnore
        var password: String,


        var active: Boolean = true,

        @ElementCollection(targetClass = Authority::class, fetch = FetchType.EAGER)
        @Enumerated(EnumType.STRING) // Possibly optional (I'm not sure) but defaults to ORDINAL.
        @CollectionTable(name = "authority")
        @Column(name = "authority") // Column name in person_interest
        override var authorities: MutableSet<Authority>? = mutableSetOf(),

        @CreatedBy
        @JsonIgnore
        var createdBy: Int? = null,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @LastModifiedBy
        @JsonIgnore
        var lastModifiedBy: Int? = null,

        @LastModifiedDate
        var lastModifiedDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @Column(name = "confirmation_code")
        val confirmationCode: String = Utils.generateID(32)

) : IUser<Authority> {
    // TODO -  Remove this and handle No Default Constructor found
    constructor() : this(null, "", "",
            "", "", "", true, mutableSetOf())

    @JsonIgnore
    override fun getPrincipal(): String = id.toString()

    @JsonIgnore
    override fun canAuthenticate(): Boolean = this.active

    @JsonIgnore
    override fun getSecret(): String = password

}