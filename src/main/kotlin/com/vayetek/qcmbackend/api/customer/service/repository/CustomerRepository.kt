package com.vayetek.qcmbackend.api.customer.service.repository

import com.vayetek.qcmbackend.api.customer.model.Customer
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CustomerRepository : JpaRepository<Customer, Int> {
    fun findOneByUsername(username: String): Optional<Customer>
}