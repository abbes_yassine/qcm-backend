package com.vayetek.qcmbackend.api.qcm.service.repository

import com.vayetek.qcmbackend.api.qcm.model.Question
import org.springframework.data.jpa.repository.JpaRepository

interface QuestionRepository : JpaRepository<Question, Int> {
}