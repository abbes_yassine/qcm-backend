package com.vayetek.qcmbackend.api.qcm.service

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.qcm.model.Qcm
import com.vayetek.qcmbackend.api.qcm.model.Question
import com.vayetek.qcmbackend.api.qcm.model.Response
import com.vayetek.qcmbackend.api.qcm.service.repository.QcmRepository
import com.vayetek.qcmbackend.api.qcm.service.repository.QuestionRepository
import com.vayetek.qcmbackend.api.qcm.service.repository.ResponseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QcmService : IQcmService {


    @Autowired
    lateinit var repository: QcmRepository

    @Autowired
    lateinit var questionRepository: QuestionRepository

    @Autowired
    lateinit var responseRepository: ResponseRepository


    override fun addResponse(itemResponse: Response): Response {
        return responseRepository.save(itemResponse)
    }

    override fun addQuestion(question: Question): Question {
        return questionRepository.save(question)
    }

    override fun add(qcm: Qcm): Qcm {
        return repository.save(qcm)
    }

    override fun getAllByUser(user: Customer): List<Qcm>? {
        return repository.getByUser(user)
    }

    override fun getById(qcmId: Int): Qcm? {
        return repository.findById(qcmId)
                .orElseGet({ null })
    }

    override fun delete(qcmId: Int) {
        return repository.deleteById(qcmId)
    }

    override fun update(qcm: Qcm, request: Qcm): Qcm {
        qcm.let {
            it.title = request.title
            it.description = request.description
            it
        }
        return repository.save(qcm)
    }


    override fun deleteQuestion(itemQuestion: Question) {
        return questionRepository.delete(itemQuestion)
    }

}