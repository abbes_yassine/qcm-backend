package com.vayetek.qcmbackend.api.qcm

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.qcm.model.Qcm
import com.vayetek.qcmbackend.api.qcm.model.Question
import com.vayetek.qcmbackend.api.qcm.service.IQcmService
import com.vayetek.qcmbackend.api.user.service.IUserService
import com.vayetek.qcmbackend.api.welcome.SimpleResponse
import com.vayetek.qcmbackend.libraries.constants.DomainExceptions
import com.vayetek.qcmbackend.libraries.errors.APIException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/qcm")
class QcmResource {

    val LOG: Logger = LoggerFactory.getLogger(QcmResource::class.java)


    @Autowired
    lateinit var qcmService: IQcmService

    @Autowired
    lateinit var userService: IUserService<Customer>


    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    fun getByUser(): List<Qcm>? {
        val user = userService.getConnectedUser() ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)
        return qcmService.getAllByUser(user)
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun getById(@PathVariable("id") qcmId: Int): Qcm {
        val user = userService.getConnectedUser() ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        val qcm = qcmService.getById(qcmId) ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        if (qcm.user != null && user.id != qcm.user?.id) {
            throw APIException(DomainExceptions.USER_NOT_PERMITTED)
        }

        return qcm
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    fun add(@RequestBody request: Qcm): SimpleResponse {
        val user = userService.getConnectedUser() ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        request.user = user

        val qcm = qcmService.add(request)

        addQuestions(qcm, request.questions);

        return SimpleResponse("Add QCM success")
    }


    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable("id") qcmId: Int): SimpleResponse {
        val user = userService.getConnectedUser() ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        val qcm = qcmService.getById(qcmId) ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        if (qcm.user != null && user.id != qcm.user?.id) {
            throw APIException(DomainExceptions.USER_NOT_PERMITTED)
        }

        qcmService.delete(qcmId)

        return SimpleResponse("Delete QCM success")
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun update(@PathVariable("id") qcmId: Int, @RequestBody request: Qcm): SimpleResponse {
        val user = userService.getConnectedUser() ?: throw APIException(DomainExceptions.ELEMENT_NOT_FOUND)

        if(request.user != null && request.user?.id != user.id){
            throw APIException(DomainExceptions.USER_NOT_PERMITTED)
        }

        val qcm = qcmService.add(request)

        addQuestions(qcm, request.questions);

        return SimpleResponse("Update QCM success")
    }


    private fun addQuestions(qcm: Qcm, questions: List<Question>?) {
        for (itemQuestion in questions!!) {
            itemQuestion.qcm = qcm
            val question = qcmService.addQuestion(itemQuestion)
            for (itemResponse in itemQuestion.responses!!) {
                itemResponse.question = question
                qcmService.addResponse(itemResponse)
            }
        }
    }


}