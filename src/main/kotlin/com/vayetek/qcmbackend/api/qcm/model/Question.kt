package com.vayetek.qcmbackend.api.qcm.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Question")
data class Question(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        var title: String,

        var percentage: Float,

        @ManyToOne(fetch = FetchType.EAGER)
        @JsonIgnore
        var qcm: Qcm? = null,

        @OneToMany(mappedBy = "question",
                cascade = [CascadeType.REMOVE],
                orphanRemoval = true)
        var responses: List<Response>? = null,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()

) {
    constructor() : this(title = "", percentage = 0F)
}