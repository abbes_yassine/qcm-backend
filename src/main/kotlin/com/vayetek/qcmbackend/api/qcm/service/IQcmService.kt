package com.vayetek.qcmbackend.api.qcm.service

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.qcm.model.Qcm
import com.vayetek.qcmbackend.api.qcm.model.Question
import com.vayetek.qcmbackend.api.qcm.model.Response

interface IQcmService {
    fun getAllByUser(user: Customer): List<Qcm>?
    fun add(qcm: Qcm): Qcm
    fun addQuestion(question: Question): Question
    fun addResponse(itemResponse: Response): Response
    fun getById(qcmId: Int): Qcm?
    fun delete(qcmId: Int)
    fun update(qcm: Qcm, request: Qcm): Qcm
    fun deleteQuestion(itemQuestion: Question)
}