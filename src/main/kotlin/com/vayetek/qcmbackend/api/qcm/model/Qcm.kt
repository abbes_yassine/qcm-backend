package com.vayetek.qcmbackend.api.qcm.model

import com.vayetek.qcmbackend.api.customer.model.Customer
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Qcm")
data class Qcm(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        var title: String,

        @Column(length = 1000)
        var description: String,

        @OneToMany(mappedBy = "qcm",
                cascade = [CascadeType.REMOVE],
                orphanRemoval = true)
        var questions: List<Question>? = null,

        @ManyToOne
        var user: Customer? = null,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()
) {
    constructor() : this(title = "", description = "")
}