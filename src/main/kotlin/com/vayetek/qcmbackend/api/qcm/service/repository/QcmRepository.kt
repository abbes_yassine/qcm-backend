package com.vayetek.qcmbackend.api.qcm.service.repository

import com.vayetek.qcmbackend.api.customer.model.Customer
import com.vayetek.qcmbackend.api.qcm.model.Qcm
import org.springframework.data.jpa.repository.JpaRepository

interface QcmRepository : JpaRepository<Qcm, Int> {
    fun getByUser(user: Customer): List<Qcm>?
}