package com.vayetek.qcmbackend.api.qcm.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*


@Entity(name = "Response")
data class Response(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        var title: String,

        var percentage: Float,

        @ManyToOne(fetch = FetchType.EAGER)
        @JsonIgnore
        var question: Question? = null,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()
) {
    constructor() : this(title = "", percentage = 0F)
}