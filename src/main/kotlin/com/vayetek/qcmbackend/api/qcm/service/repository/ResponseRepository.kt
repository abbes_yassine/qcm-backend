package com.vayetek.qcmbackend.api.qcm.service.repository

import com.vayetek.qcmbackend.api.qcm.model.Response
import org.springframework.data.jpa.repository.JpaRepository

interface ResponseRepository : JpaRepository<Response, Int> {
}