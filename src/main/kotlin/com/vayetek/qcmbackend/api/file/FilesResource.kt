package com.vayetek.qcmbackend.api.file


import com.vayetek.qcmbackend.data.mongo.files.DBFile
import com.vayetek.qcmbackend.data.mongo.files.DBFileService
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import com.vayetek.qcmbackend.libraries.utilis.ValidationConstants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile


@RestController
@RequestMapping("/api/files")
@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
class FilesResource {

    @Autowired
    lateinit var fileService: DBFileService

    @GetMapping("/{id:" + ValidationConstants.MONGOID_REGEX + "}/**")
    @ResponseStatus(HttpStatus.OK)
    fun getFile(@PathVariable("id") id: String): ResponseEntity<InputStreamResource> {
        val file = fileService.get(id)

        val res = InputStreamResource(file.inputStream ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND))
        val httpHeaders = HttpHeaders()
        httpHeaders.contentLength = file.length
        httpHeaders.contentType = MediaType.parseMediaType(file.contentType
                ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND))
        return ResponseEntity(res, httpHeaders, HttpStatus.OK)
    }


    @PutMapping("/upload")
    @ResponseStatus(HttpStatus.OK)
    fun updateFile(@RequestPart(value = "file") file: MultipartFile): DBFile {
        return fileService.save(file, null)
    }
}
