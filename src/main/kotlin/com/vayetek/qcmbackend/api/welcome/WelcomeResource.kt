package com.vayetek.qcmbackend.api.welcome

import com.vayetek.qcmbackend.api.welcome.model.response.WelcomeResponse
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.net.InetAddress

@RestController
@RequestMapping("/")
class WelcomeResource {

    @Autowired
    lateinit var env: Environment

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun welcome(): WelcomeResponse {
        val protocol = "http"
        return WelcomeResponse(env.getProperty("spring.application.name")
                ?: throw APIException(BaseExceptions.RESOURCE_NOT_FOUND),
                "green",
                "$protocol://localhost:${env.getProperty("server.port") ?: 8080}",
                "$protocol://${InetAddress.getLocalHost().hostAddress}:${env.getProperty("server.port")
                        ?: 8080}")
    }

}