package com.vayetek.qcmbackend.api.welcome

data class SimpleResponse(
        val message: String
)