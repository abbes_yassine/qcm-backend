package com.vayetek.qcmbackend.api.qcmresult.model

import com.vayetek.qcmbackend.api.qcm.model.Response
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Item_Response")
data class ItemResponse(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        @ManyToOne
        var questionResult: QuestionResult,

        @ManyToOne
        var response: Response,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()
)