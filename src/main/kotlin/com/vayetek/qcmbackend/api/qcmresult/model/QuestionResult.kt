package com.vayetek.qcmbackend.api.qcmresult.model

import com.vayetek.qcmbackend.api.qcm.model.Question
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Question_Result")
data class QuestionResult(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        var note: Float,

        @ManyToOne
        var qcmResult: QcmResult,

        @ManyToOne
        var question: Question,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()
)