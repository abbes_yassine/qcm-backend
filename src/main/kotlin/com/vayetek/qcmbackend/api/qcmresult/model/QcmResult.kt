package com.vayetek.qcmbackend.api.qcmresult.model

import com.vayetek.qcmbackend.api.qcm.model.Qcm
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Entity(name = "Qcm_Result")
data class QcmResult(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        var note: Float,

        @Column(name = "num_comp")
        var numComp: Int,

        @ManyToOne
        var qcm: Qcm,

        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime()
)