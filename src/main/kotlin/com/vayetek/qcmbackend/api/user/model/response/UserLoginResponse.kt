package com.vayetek.qcmbackend.api.welcome.model.response

data class UserLoginResponse(val message: String)