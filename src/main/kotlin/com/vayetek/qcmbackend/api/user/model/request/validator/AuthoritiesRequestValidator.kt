package com.vayetek.qcmbackend.api.user.model.request.validator

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass


@MustBeDocumented
@Constraint(validatedBy = [(AuthoritiesRequestValidatorImpl::class)])
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class AuthoritiesRequestValidator(val message: String = "Can register as Administrator", val groups: Array<KClass<*>> = [], val payload: Array<KClass<out Payload>> = [])