package com.vayetek.qcmbackend.api.user.model.request

import org.jetbrains.annotations.NotNull

class LoginRequest {

    @NotNull
    lateinit var username: String
    
    @NotNull
    lateinit var password: String
}