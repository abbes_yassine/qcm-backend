package com.vayetek.qcmbackend.api.user.service

import com.vayetek.qcmbackend.libraries.constants.Authority
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface IUserService<T> {
    fun getConnectedUser(): T?
    fun findOneByUsername(username: String): T?
    fun findOneByPrincipal(principal: String): T?
    fun isUsernameAvailable(username: String, connected: T? = null): Boolean
    fun get(page: Pageable): Page<T>
    fun create(toRegister: T, authorities: MutableSet<Authority>? = null): T
    fun update(base: T, update: T): T
    fun delete(user: T)
}
