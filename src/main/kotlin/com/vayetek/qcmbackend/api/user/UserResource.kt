package com.vayetek.qcmbackend.api.user

import com.vayetek.qcmbackend.api.user.model.User
import com.vayetek.qcmbackend.api.user.model.request.LoginRequest
import com.vayetek.qcmbackend.api.user.model.request.RegistrationRequest
import com.vayetek.qcmbackend.api.user.service.UserService
import com.vayetek.qcmbackend.libraries.constants.DomainExceptions
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.security.authentication.IAuthentificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/user")
@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
class UserResource {


    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var jwtAuthentificationService: IAuthentificationService


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<User> {
        return userService.get(page)
    }

    @PostMapping("/guest/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerCustomer(@Valid @RequestBody creation: RegistrationRequest): User {

        val request = creation.toUser()

        if (!userService.isUsernameAvailable(request.username))
            throw APIException(DomainExceptions.ACCOUNT_ALREADY_EXISTS)

        val account = userService.create(request)
        return userService.create(account)
    }


    @PostMapping("/guest/login")
    @ResponseStatus(HttpStatus.OK)
    fun authenticate(@Valid @RequestBody login: LoginRequest): IAuthentificationService.AuthenticationResult {
        val authenticationToken = UsernamePasswordAuthenticationToken(login.username, login.password)
        return jwtAuthentificationService.authenticate(authenticationToken).let {
            it.user = userService.getConnectedUser()
            it
        }
    }
}