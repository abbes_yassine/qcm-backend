package com.vayetek.qcmbackend.api.user.model.request

import com.vayetek.qcmbackend.api.user.model.User
import com.vayetek.qcmbackend.api.user.model.request.validator.AuthoritiesRequestValidator
import com.vayetek.qcmbackend.libraries.constants.Authority
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

class RegistrationRequest {

    @Email
    @NotNull
    lateinit var username: String

    @NotNull
    lateinit var password: String

    @NotNull
    @AuthoritiesRequestValidator
    var authorities: MutableSet<Authority>? = mutableSetOf()

    fun toUser() = User(
            username.trim().toLowerCase(),
            password,
            authorities = authorities)
}