package com.vayetek.qcmbackend.api.user.service.repository

import com.vayetek.qcmbackend.api.user.model.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository : MongoRepository<User, String> {

    fun findOneByUsername(username: String): User?

    fun findByIdIn(ids: List<String>): List<User>

}
