package com.vayetek.qcmbackend.api.user.model.request.validator

import com.vayetek.qcmbackend.libraries.constants.Authority
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class AuthoritiesRequestValidatorImpl : ConstraintValidator<AuthoritiesRequestValidator, MutableSet<Authority>> {
    override fun initialize(constraintAnnotation: AuthoritiesRequestValidator?) {

    }

    override fun isValid(value: MutableSet<Authority>?, context: ConstraintValidatorContext?): Boolean {
        value ?: return true
        value.forEach {
            if (it == Authority.ADMIN) {
                return false
            }
        }
        return true
    }
}