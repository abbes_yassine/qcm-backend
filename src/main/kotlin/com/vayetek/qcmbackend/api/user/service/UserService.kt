package com.vayetek.qcmbackend.api.user.service

import com.vayetek.qcmbackend.api.user.model.User
import com.vayetek.qcmbackend.api.user.service.repository.UserRepository
import com.vayetek.qcmbackend.libraries.constants.Authority
import com.vayetek.qcmbackend.libraries.errors.APIException
import com.vayetek.qcmbackend.libraries.errors.BaseExceptions
import com.vayetek.qcmbackend.libraries.security.jwt.SecurityUtils
import com.vayetek.qcmbackend.libraries.utilis.EntityUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(name = ["spring.database"], havingValue = "mongo")
class UserService : IUserService<User> {

    val LOG: Logger = LoggerFactory.getLogger(UserService::class.java)

    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder


    override fun findOneByPrincipal(principal: String): User? = findOneByUsername(principal)

    override fun getConnectedUser(): User? {
        val login = SecurityUtils.getCurrentUserLogin() ?: return null
        return repository.findOneByUsername(login)
    }

    fun getConnectedUserOrThrow(): User {
        return getConnectedUser() ?: throw APIException(BaseExceptions.UNAUTHORIZED)
    }

    override fun findOneByUsername(username: String): User? {
        return repository.findOneByUsername(username)
    }

    fun findOne(id: String): User? {
        return repository.findById(id).get()
    }

    fun findAll(ids: List<String>): List<User> {
        return repository.findByIdIn(ids)
    }

    override fun get(page: Pageable): Page<User> {
        return repository.findAll(page)
    }

    override fun isUsernameAvailable(username: String, connected: User?): Boolean {
        val existing = this.findOneByUsername(username)
        return existing == null || existing.id == connected?.id
    }

    override fun create(toRegister: User, authorities: MutableSet<Authority>?): User {
        LOG.info("Creating $authorities user ${toRegister.username}")

        val user = toRegister.copy(password = passwordEncoder.encode(toRegister.password))
        if (authorities != null) {
            val userAuthorities = user.authorities ?: mutableSetOf()
            userAuthorities.addAll(authorities)
            user.authorities = userAuthorities
        }
        return repository.save(user)
    }

    override fun update(base: User, update: User): User {
        LOG.info("Updating User ${base.username}")

        val entity = if (update != null) {
            if (update.username.isNotBlank())
                base.username = update.username
            if (update.password.isNotBlank())
                base.password = passwordEncoder.encode(update.password)
            EntityUtils.update(base, update, User::class.java)
        } else base


        return repository.save(entity)
    }


    override fun delete(user: User) {
        repository.delete(user)
    }
}
