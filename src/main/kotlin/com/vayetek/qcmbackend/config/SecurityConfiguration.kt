package com.vayetek.qcmbackend.config

import com.vayetek.qcmbackend.libraries.security.IHttpSecurityConfigurer
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.stereotype.Component


@Component
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration : IHttpSecurityConfigurer {

    override fun configure(http: HttpSecurity?) {
        http ?: return

        http
                .authorizeRequests()
                .antMatchers("/api/user/guest/**").permitAll()
                .antMatchers("/api/files/**").permitAll()
                .antMatchers("/api/payment/**").permitAll()
                .antMatchers("/api/customer/guest/**").permitAll()
                .antMatchers("/api/**").authenticated()
    }
}
