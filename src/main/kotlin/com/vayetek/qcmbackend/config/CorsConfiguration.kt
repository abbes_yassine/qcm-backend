package com.vayetek.qcmbackend.config

import com.vayetek.qcmbackend.libraries.security.IHttpSecurityConfigurer
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class CorsConfiguration : WebMvcConfigurer, IHttpSecurityConfigurer {
    override fun configure(http: HttpSecurity?) {
        http ?: return
        http.cors()
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .allowedHeaders("*")
                .allowCredentials(false)
                .maxAge(3600)

    }

}
