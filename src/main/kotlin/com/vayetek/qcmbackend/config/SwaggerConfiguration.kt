package com.vayetek.qcmbackend.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ParameterBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.schema.ModelRef
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    /**
     * API Doc
     * http(s)://HOST:PORT/v2/api-docs
     */
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().globalOperationParameters(arrayListOf(
                        ParameterBuilder()
                                .name("Authorization")
                                .description("Authorization")
                                .modelRef(ModelRef("string"))
                                .parameterType("header")
                                .required(false)
                                .build(),
                        ParameterBuilder()
                                .name("page")
                                .description("page")
                                .modelRef(ModelRef("Int"))
                                .parameterType("query")
                                .required(false)
                                .build(),
                        ParameterBuilder()
                                .name("size")
                                .description("size")
                                .modelRef(ModelRef("Int"))
                                .parameterType("query")
                                .required(false)
                                .build()))
    }
}
