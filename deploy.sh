#make sure to build the project './gradlew clean build'
app="com.vayetek:backend-template-spring"

./gradlew copyDockerFile

docker build ./build/libs/ -t "$app"

docker-compose up -d